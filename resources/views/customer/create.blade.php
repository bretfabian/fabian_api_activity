@extends('customer.layout')

@section('content')

	<div class="row bg-dark text-white">

		<div class="col-lg-12 margin-tb">

			<div class="pull-left">

				<h2> New Comment</h2>
			</div>

			<div class="pull-right">

				<a class="btn btn-warning" href="{{route('customer.index')}}">Back</a>

			</div>

		</div>

		@if ($errors->any())

			<div class="alert alert-danger">

				<strong>UHHMMMM</strong> you messed up.<br><br>

				<ul>

					@foreach($errors->all() as$error)

					<li>{{$error}}</li>

					@endforeach

				</ul>

			</div>

		@endif

			<form action="{{route('customer.store')}}" method="POST">

				@csrf

				<div class="row">

					<div class="col-cs-12 col-sm-12 col-md-12">

						<div class="form-group">

							<strong>CustomerName:</strong>

								<input type="text" name="custname" class="form-control" placeholder="custname">

						</div>
					</div>

					<div class="col-cs-12 col-sm-12 col-md-12">

						<div class="form-group">

							<strong>Comment</strong>

							<input type="text" name="comment" class="form-control" placeholder="comment">

						</div>

					</div>

					<div class="col-xs-12 col-sm-12 col-md-12">

						<div class="form-group">

							<strong>Rating</strong>

							<input type="text" name="rating" class="form-control" placeholder="rating">

						</div>

					</div>

					<div class="col-xs-12 col-sm-12 col-md-12 text-center">

						<button type="submit" class="btn btn-warning">Submit</button>

					</div>
				</div>

			</form>

			@endsection

