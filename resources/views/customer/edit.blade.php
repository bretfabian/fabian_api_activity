@extends('customer.layout')

@section('content')

	<div class="row">

		<div class="col-lg-12 margin-tb bg-dark text-white">

			<div class="pull-left">

				<h2>Edit Comment</h2>
			</div>

			<div class="pull-right">

				<a class="btn btn-warning" href="{{route('customer.index')}}">Back</a>

			</div>
		</div>

	</div>

	@if($errors->any())

		<div class="alert alert-danger">

			<strong>UHHHMMM!</strong>You messed up.<br><br>

			<ul>

				@foreach($errors->all() as $error)

				<li>{{$error}}</li>

				@endforeach
			</ul>

		</div>

	@endif


	<form action="{{route('customer.update',$customer->id)}}" method="POST" >

		@csrf

		@method('PUT')

		<div class="row bg-dark text-white">

			<div class="col-cs-12 col-sm-12 col-md-12">

				<div class="form-group">

					<strong>CustName:</strong>

					<input type="text" name="custname" value="{{$customer->custname}}" class="form-control" placeholder="Name">

				</div>
			</div>

			<div class="col-cs-12 col-sm-12 col-md-12">

				<div class="form-group">

					<strong>Comment</strong>

					<input type="text" name="comment" value="{{$customer->comment}}" class="form-control" placeholder="comment">

				</div>

			</div>

			<div class="col-cs-12 col-sm-12 col-md-12">

				<div class="form-group">

					<strong>Rating</strong>

					<input type="text" name="rating" value="{{$customer->rating}}" class="form-control" placeholder="rating">

				</div>

			</div>


			<div class="col-cs-12 col-sm-12 col-md-12 text-center">

				<button type="submit" class="btn btn-warning">Submit</button>

			</div>

		</div>

	</form>

	@endsection

