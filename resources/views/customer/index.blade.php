@extends('customer.layout')

@section('content')


<div class="pull-left bg-dark text-white p-2">

	<h2> Customer Comments Wall</h2>
</div>


<div class="row">
	<div class="col-lg-12 margin- bg-dark">

		<div class="pull-right">

         <a class="btn btn-warning" href="{{route('customer.create')}}"> New Comment</a>

		</div>
	</div>
</div>

	@if($message=Session::get('success'))

	<div class="alert alert-success">

		<p>{{$message}}</p>
	</div>

@endif

	<table class="table table-bordered bg-dark text-white">

		<tr>

			<th>Que</th>

			<th>Name</th>

			<th>Comment</th>

			<th>Rating</th>

			<th width="280px">Action</th>
		</tr>

@foreach ($customer as $customer)

	<tr>

		<td>{{++$i}}</td>

		<td>{{$customer->custname}}</td>
		<td>{{$customer->comment}}</td>
		<td>{{$customer->rating}}</td>

		<td>

			<form action="{{route('customer.destroy',$customer->id)}}" method="POST">

			
			<a class= "btn btn-success" href="{{route('customer.edit',$customer->id)}}">Edit
			</a>

	@csrf
	@method('DELETE')

				<button type="submit" class="btn btn-danger">Delete</button>

			</form>
		</td>

	</tr>

@endforeach
</table>